/**
 * This package contains all viptool Petri net data classes.
 * 
 * @author thomas
 */
package de.fuhagen.sttp.mopebs.data.pnml;