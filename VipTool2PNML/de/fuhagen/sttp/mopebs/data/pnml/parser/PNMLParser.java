package de.fuhagen.sttp.mopebs.data.pnml.parser;

import java.io.File;
import java.util.Vector;

import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.Characters;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;

import de.fuhagen.sttp.mopebs.data.graph.DragPoint;
import de.fuhagen.sttp.mopebs.data.pnml.Edge;
import de.fuhagen.sttp.mopebs.data.pnml.PetriNet;
import de.fuhagen.sttp.mopebs.data.pnml.Place;
import de.fuhagen.sttp.mopebs.data.pnml.Transition;
import de.fuhagen.sttp.mopebs.util.StAXParser;

/**
 * This class implements a basic parser for pnml files.
 * 
 * @author thomas
 */
public class PNMLParser extends StAXParser {

    /**
     * Current XML path.
     */
    private Vector<String>   path;
    /**
     * Found nets.
     */
    private Vector<PetriNet> nets;

    /**
     * Current {@link PetriNet}.
     */
    private PetriNet         net;
    /**
     * Current {@link Transition}.
     */
    private Transition       transition;
    /**
     * Current {@link Place}.
     */
    private Place            place;
    /**
     * Current {@link Edge}.
     */
    private Edge             edge;

    /**
     * Creates a new {@link PNMLParser} for the given {@link File}.
     * 
     * @param pnmlfile
     *            PNML {@link File}.
     */
    public PNMLParser(File pnmlfile) {
        super(pnmlfile);
        path = new Vector<String>();
        nets = new Vector<PetriNet>();
    }

    /**
     * This method returns the parsed {@link PetriNet}s.
     * 
     * @return {@link Vector} of {@link PetriNet}s
     */
    public Vector<PetriNet> getNets() {
        return nets;
    }

    @Override
    public void startElement(StartElement start) {
        String name = start.getName().getLocalPart();
        name = name.toLowerCase();
        path.addElement(name);

        if (name.equals("net")) {
            net = new PetriNet(file);
            nets.add(net);
        } else if (name.equals("transition")) {
            transition = new Transition();
            net.getTransitions().add(transition);
        } else if (name.equals("place")) {
            place = new Place();
            net.getPlaces().add(place);
        } else if (name.equals("arc")) {
            edge = new Edge();
            net.getEdges().add(edge);
        }
    }

    @Override
    public void attribute(final Attribute attribute) {
        String name = attribute.getName().getLocalPart().toLowerCase();

        if (name.equals("id") && path.lastElement().equals("net")) {
            net.setId(attribute.getValue());
        } else if (name.equals("id") && path.lastElement().equals("transition")) {
            transition.getIdProperty().set(attribute.getValue());
        } else if (name.equals("x") && path.lastElement().equals("position")
                && path.contains("transition")) {
            transition.setX((int) Double.parseDouble(attribute.getValue()));
        } else if (name.equals("y") && path.lastElement().equals("position")
                && path.contains("transition")) {
            transition.setY((int) Double.parseDouble(attribute.getValue()));
        } else if (name.equals("id") && path.lastElement().equals("place")) {
            place.getIdProperty().set(attribute.getValue());
        } else if (name.equals("x") && path.lastElement().equals("position")
                && path.contains("place")) {
            place.setX((int) Double.parseDouble(attribute.getValue()));
        } else if (name.equals("y") && path.lastElement().equals("position")
                && path.contains("place")) {
            place.setY((int) Double.parseDouble(attribute.getValue()));
        } else if (name.equals("id") && path.lastElement().equals("arc")) {
            edge.getIdProperty().set(attribute.getValue());
        } else if (name.equals("source") && path.lastElement().equals("arc")) {
            edge.startid = attribute.getValue();
        } else if (name.equals("target") && path.lastElement().equals("arc")) {
            edge.endid = attribute.getValue();
        } else if (name.equals("x") && path.lastElement().equals("position")
                && path.contains("arc")) {
            edge.dragPointX.add((int) Double.parseDouble(attribute.getValue()));
        } else if (name.equals("y") && path.lastElement().equals("position")
                && path.contains("arc")) {
            edge.dragPointY.add((int) Double.parseDouble(attribute.getValue()));
        } else if (name.equals("x") && path.lastElement().equals("offset")
                && path.contains("transition")) {
            transition.setOffsetX((int) Double
                    .parseDouble(attribute.getValue()));
        } else if (name.equals("y") && path.lastElement().equals("offset")
                && path.contains("transition")) {
            transition.setOffsetY((int) Double
                    .parseDouble(attribute.getValue()));
        } else if (name.equals("x") && path.lastElement().equals("offset")
                && path.contains("place")) {
            place.setOffsetX((int) Double.parseDouble(attribute.getValue()));
        } else if (name.equals("y") && path.lastElement().equals("offset")
                && path.contains("place")) {
            place.setOffsetY((int) Double.parseDouble(attribute.getValue()));
        } else if (name.equals("x") && path.lastElement().equals("offset")
                && path.contains("arc")) {
            edge.setOffsetX((int) Double.parseDouble(attribute.getValue()));
        } else if (name.equals("y") && path.lastElement().equals("offset")
                && path.contains("arc")) {
            edge.setOffsetY((int) Double.parseDouble(attribute.getValue()));
        }
    }

    @Override
    public void endElement(final EndElement end) {
        path.remove(path.size() - 1);

        String name = end.getName().getLocalPart();
        name = name.toLowerCase();
        if (name.equals("arc")) {
            int dpc = Math.min(edge.dragPointX.size(), edge.dragPointY.size());
            for (int i = 0; i < dpc; i++) {
                DragPoint dragPoint = new DragPoint(edge.dragPointX.get(i),
                        edge.dragPointY.get(i), i * 10 + 10);
                edge.getDragPoints().add(dragPoint);
            }
            edge.dragPointX = null;
            edge.dragPointY = null;
        } else if (name.equals("net")) {
            Vector<Edge> invalidEdges = new Vector<Edge>();

            for (Edge edge : net.getEdges()) {
                for (Transition t : net.getTransitions()) {
                    if (t.getId().equals(edge.startid)) {
                        edge.setStart(t);
                        edge.startid = null;
                        break;
                    }
                    if (t.getId().equals(edge.endid)) {
                        edge.setEnd(t);
                        edge.endid = null;
                        break;
                    }
                }
                for (Place p : net.getPlaces()) {
                    if (p.getId().equals(edge.startid)) {
                        edge.setStart(p);
                        edge.startid = null;
                        break;
                    }
                    if (p.getId().equals(edge.endid)) {
                        edge.setEnd(p);
                        edge.endid = null;
                        break;
                    }
                }

                if (edge.startid != null || edge.endid != null) {
                    System.err.println("Invalid edge " + edge + " ("
                            + edge.getId() + ")");
                    invalidEdges.add(edge);
                }
            }

            net.getEdges().removeAll(invalidEdges);
        }
    }

    @Override
    public void characters(final Characters characters) {
        if (path.contains("transition")
                && (path.lastElement().equals("value") || path.lastElement()
                        .equals("text"))) {
            transition.setLabel(characters.getData());
        } else if (path.contains("place")
                && !path.contains("initialmarking")
                && (path.lastElement().equals("value") || path.lastElement()
                        .equals("text"))) {
            place.setLabel(characters.getData());
        } else if (path.contains("place")
                && path.contains("initialmarking")
                && (path.lastElement().equals("value") || path.lastElement()
                        .equals("text"))) {
            place.setMarking((int) Double.parseDouble(characters.getData()));
        } else if (path.contains("arc")
                && (path.lastElement().equals("value") || path.lastElement()
                        .equals("text"))) {
            edge.setWeight((int) Double.parseDouble(characters.getData()));
        }

    }
}
