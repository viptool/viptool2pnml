/**
 * This package contains the parsers which read
 * {@link de.fuhagen.sttp.mopebs.data.pnml.PetriNet}s
 * 
 * @author thomas
 */
package de.fuhagen.sttp.mopebs.data.pnml.parser;