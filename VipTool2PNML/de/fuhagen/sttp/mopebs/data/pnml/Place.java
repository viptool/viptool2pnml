package de.fuhagen.sttp.mopebs.data.pnml;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import de.fuhagen.sttp.mopebs.data.graph.Node;

/**
 * This class represents a {@link Place} of a {@link PetriNet}.
 * 
 * @author thomas
 */
public class Place extends Node {

    /**
     * Border length of place visualization.
     */
    public static final int PLACE_SIZE = 32;

    /**
     * Marking of this place.
     */
    private IntegerProperty marking;

    /**
     * Creates a new {@link Place} with label "Place".
     */
    public Place() {
        super();

        setWidth(PLACE_SIZE);
        setHeigth(PLACE_SIZE);
        setOffsetX(0);
        setOffsetY(0);
        setLabel("Place");

        marking = new SimpleIntegerProperty();
    }

    /**
     * This method sets the marking of this place to the given token count.
     * 
     * @param tokenCount
     *            marking of {@link Place} as {@link Integer}
     */
    public void setMarking(int tokenCount) {
        this.marking.set(tokenCount);
    }

    /**
     * This method returns the marking of this place.
     * 
     * @return token count as {@link Integer}
     */
    public int getMarking() {
        return marking.get();
    }

    /**
     * This method retruns the marking of this {@link Place} as
     * {@link IntegerProperty}.
     * 
     * @return marking as {@link IntegerProperty}
     */
    public IntegerProperty getMarkingProperty() {
        return marking;
    }
}
