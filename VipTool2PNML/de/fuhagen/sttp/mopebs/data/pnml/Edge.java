package de.fuhagen.sttp.mopebs.data.pnml;

import de.fuhagen.sttp.mopebs.data.graph.Arc;

/**
 * This class represents a {@link PetriNet} {@link Edge}.
 * 
 * @author thomas
 */
public class Edge extends Arc {

    /**
     * Creates a new Edge with weight 1.
     */
    public Edge() {
        setWeight(1);
    }

}
