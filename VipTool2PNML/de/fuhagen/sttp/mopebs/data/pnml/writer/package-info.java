/**
 * This package contains the writer of
 * {@link de.fuhagen.sttp.mopebs.data.pnml.PetriNet}s.
 * 
 * @author thomas
 */
package de.fuhagen.sttp.mopebs.data.pnml.writer;