package de.fuhagen.sttp.mopebs.data.pnml.writer;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.sun.org.apache.xml.internal.serialize.OutputFormat;
import com.sun.org.apache.xml.internal.serialize.XMLSerializer;

import de.fuhagen.sttp.mopebs.data.graph.DragPoint;
import de.fuhagen.sttp.mopebs.data.pnml.Edge;
import de.fuhagen.sttp.mopebs.data.pnml.PetriNet;
import de.fuhagen.sttp.mopebs.data.pnml.Place;
import de.fuhagen.sttp.mopebs.data.pnml.Transition;

/**
 * This Petri net writer stores the {@link PetriNet} as PNML file.
 * 
 * @author thomas
 */
public class NoOffsetPNMLWriter {

	/**
	 * {@link PetriNet} to write.
	 */
	private PetriNet net;
	/**
	 * {@link File} for {@link PetriNet}.
	 */
	private File file;

	/**
	 * Creates a new {@link NoOffsetPNMLWriter} for the given {@link PetriNet}.
	 * 
	 * @param net
	 *            {@link PetriNet} to write
	 */
	public NoOffsetPNMLWriter(PetriNet net) {
		super();

		this.net = net;
		this.file = net.getPnmlFile();
	}

	/**
	 * Creates a new {@link NoOffsetPNMLWriter} for the given {@link PetriNet} and the
	 * given {@link File}.
	 * 
	 * @param net
	 *            {@link PetriNet} to write
	 * @param file
	 *            {@link File} for {@link PetriNet}
	 */
	public NoOffsetPNMLWriter(PetriNet net, File file) {
		super();

		this.net = net;
		this.file = file;
	}

	/**
	 * This method saves the {@link PetriNet} to the {@link File}.
	 */
	public void save() {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder;
		try {
			builder = factory.newDocumentBuilder();
			Document document = builder.newDocument();

			addPetriNet(document);

			OutputFormat format = new OutputFormat(document);
			format.setEncoding("UTF-8");
			format.setIndenting(true);
			format.setIndent(5);
			Writer out = new StringWriter();
			XMLSerializer serializer = new XMLSerializer(out, format);
			try {
				serializer.serialize(document);
				String result = out.toString();

				FileWriter fw = new FileWriter(file, false);
				fw.write(result);
				fw.flush();
				fw.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		}
	}

	/**
	 * This method adds the {@link PetriNet} data to the {@link Document}.
	 * 
	 * @param document
	 *            {@link Document} to add data to
	 */
	private void addPetriNet(Document document) {
		Element root = document.createElement("pnml");
		document.appendChild(root);

		Element n = document.createElement("net");
		n.setAttribute("id", net.getId());
		n.setAttribute("type", "http://www.pnml.org/version-2009/grammar/ptnet");
		root.appendChild(n);

		Element name = document.createElement("name");
		n.appendChild(name);
		Element text = document.createElement("text");
		text.setTextContent(net.getName());
		name.appendChild(text);

		Element page = document.createElement("page");
		page.setAttribute("id", "p1-" + System.currentTimeMillis());
		n.appendChild(page);

		net.getTransitions().forEach(t -> addTransition(document, page, t));
		net.getPlaces().forEach(p -> addPlace(document, page, p));
		net.getEdges().forEach(e -> addEdge(document, page, e));
	}

	/**
	 * This method adds the {@link Transition} to the {@link Document} as child
	 * of the given page {@link Element}.
	 * 
	 * @param document
	 *            {@link Document}
	 * @param page
	 *            parent page {@link Element}
	 * @param t
	 *            {@link Transition} to save
	 */
	private void addTransition(Document document, Element page, Transition t) {
		Element transition = document.createElement("transition");
		transition.setAttribute("id", t.getId());
		page.appendChild(transition);

		Element name = document.createElement("name");
		transition.appendChild(name);
		Element text = document.createElement("text");
		text.setTextContent(t.getLabel());
		name.appendChild(text);
		Element graphics = document.createElement("graphics");
		name.appendChild(graphics);
		Element offset = document.createElement("offset");
		offset.setAttribute("x", Integer.toString(t.getX()));
		offset.setAttribute("y",
				Integer.toString(t.getY() + t.getHeight() + 10));
		graphics.appendChild(offset);

		Element tg = document.createElement("graphics");
		transition.appendChild(tg);
		Element position = document.createElement("position");
		position.setAttribute("x", Integer.toString(t.getX()));
		position.setAttribute("y", Integer.toString(t.getY()));
		tg.appendChild(position);
	}

	/**
	 * This method adds the {@link Place} to the {@link Document} as child of
	 * the given page {@link Element}.
	 * 
	 * @param document
	 *            {@link Document}
	 * @param page
	 *            parent page {@link Element}
	 * @param p
	 *            {@link Place} to save
	 */
	private void addPlace(Document document, Element page, Place p) {
		Element place = document.createElement("place");
		place.setAttribute("id", p.getId());
		page.appendChild(place);

		Element name = document.createElement("name");
		place.appendChild(name);
		Element text = document.createElement("text");
		text.setTextContent(p.getLabel());
		name.appendChild(text);
		Element graphics = document.createElement("graphics");
		name.appendChild(graphics);
		Element offset = document.createElement("offset");
		offset.setAttribute("x", Integer.toString(p.getX()));
		offset.setAttribute("y",
				Integer.toString(p.getY() + p.getHeight() + 10));
		graphics.appendChild(offset);

		Element tg = document.createElement("graphics");
		place.appendChild(tg);
		Element position = document.createElement("position");
		position.setAttribute("x", Integer.toString(p.getX()));
		position.setAttribute("y", Integer.toString(p.getY()));
		tg.appendChild(position);

		Element initialmarking = document.createElement("initialMarking");
		place.appendChild(initialmarking);
		Element markingtext = document.createElement("text");
		markingtext.setTextContent(Integer.toString(p.getMarking()));
		initialmarking.appendChild(markingtext);
	}

	/**
	 * This method adds the {@link Edge} to the {@link Document} as child of the
	 * given page {@link Element}.
	 * 
	 * @param document
	 *            {@link Document}
	 * @param page
	 *            parent page {@link Element}
	 * @param e
	 *            {@link Edge} to save
	 */
	private void addEdge(Document document, Element page, Edge e) {
		Element edge = document.createElement("arc");
		edge.setAttribute("id", e.getId());
		edge.setAttribute("source", e.getStart().getId());
		edge.setAttribute("target", e.getEnd().getId());
		page.appendChild(edge);

		Element inscription = document.createElement("inscription");
		edge.appendChild(inscription);
		Element text = document.createElement("text");
		text.setTextContent(Integer.toString(e.getWeight()));
		inscription.appendChild(text);

		Element graphics = document.createElement("graphics");
		edge.appendChild(graphics);

		e.getSortedDragPoints().forEach(
				d -> addDragPoint(document, graphics, d));
	}

	/**
	 * This method adds the {@link DragPoint} to the {@link Document} as child
	 * of the given graphics {@link Element}.
	 * 
	 * @param document
	 *            {@link Document}
	 * @param graphics
	 *            parent graphics {@link Element}
	 * @param d
	 *            {@link DragPoint} to save
	 */
	private void addDragPoint(Document document, Element graphics, DragPoint d) {
		Element position = document.createElement("position");
		position.setAttribute("x", Integer.toString(d.getX()));
		position.setAttribute("y", Integer.toString(d.getY()));
		graphics.appendChild(position);
	}
}
