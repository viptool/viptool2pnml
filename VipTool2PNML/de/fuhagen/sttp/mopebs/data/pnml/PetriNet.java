package de.fuhagen.sttp.mopebs.data.pnml;

import java.io.File;
import java.io.IOException;
import java.util.List;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import de.fuhagen.sttp.mopebs.data.pnml.writer.NoOffsetPNMLWriter;

/**
 * This class represents a Petri net.
 * 
 * @author thomas
 */
public class PetriNet {

    /**
     * Name of the Petri net.
     */
    private final StringProperty             name;
    /**
     * ID of the Petri net.
     */
    private final StringProperty             id;

    /**
     * {@link File} which contains the Petri net.
     */
    private final File                       pnmlFile;

    /**
     * List of {@link Transition}s of this Petri net.
     */
    private final ObservableList<Transition> transitions;
    /**
     * List of {@link Place}s of this Petri net.
     */
    private final ObservableList<Place>      places;
    /**
     * List of {@link Edge}s of this Petri net.
     */
    private final ObservableList<Edge>       edges;

    /**
     * Creates a new {@link PetriNet} for the given {@link File}.
     * 
     * @param pnmlFile
     *            {@link File} which contains the {@link PetriNet}.
     */
    public PetriNet(File pnmlFile) {
        this.pnmlFile = pnmlFile;

        name = new SimpleStringProperty(pnmlFile.getName());
        id = new SimpleStringProperty("vipToolPetriNet"
                + System.currentTimeMillis());

        transitions = FXCollections.observableArrayList();
        places = FXCollections.observableArrayList();
        edges = FXCollections.observableArrayList();
    }

    /**
     * This method returns the PNML {@link File} which contains this
     * {@link PetriNet}.
     * 
     * @return PNML {@link File}
     */
    public File getPnmlFile() {
        return pnmlFile;
    }

    /**
     * This method returns the name of this Petri net.
     * 
     * @return name as {@link String}
     */
    public String getName() {
        return name.get();
    }

    /**
     * This method sets the name of this Petri net.
     * 
     * @param name
     *            name as {@link String}
     */
    public void setName(String name) {
        this.name.set(name);
    }

    /**
     * This method returns the ID of this Petri net.
     * 
     * @return ID as {@link String}
     */
    public String getId() {
        return id.getName();
    }

    /**
     * This method sets the id of this Petri net.
     * 
     * @param id
     *            id as {@link String}
     */
    public void setId(String id) {
        this.id.set(id);
    }

    /**
     * This method returns the ID of this Petri net as {@link StringProperty}.
     * 
     * @return ID as {@link StringProperty}
     */
    public StringProperty getIdProperty() {
        return id;
    }

    /**
     * This method returns the {@link Transition}s of this Petri net as
     * {@link ObservableList}.
     * 
     * @return {@link Transition}s as {@link ObservableList}
     */
    public ObservableList<Transition> getTransitions() {
        return transitions;
    }

    /**
     * This method sets the {@link Transition}s of this Petri net to the given
     * ones.
     * 
     * @param transitions
     *            {@link Transition}s as {@link List}
     */
    public void setTransitions(List<Transition> transitions) {
        this.transitions.clear();
        this.transitions.addAll(transitions);
    }

    /**
     * This method adds the given {@link Transition} to this Petri net.
     * 
     * @param transition
     *            {@link Transition} to add
     */
    public void addTransition(Transition transition) {
        transitions.add(transition);
    }

    /**
     * This method returns the {@link Place}s of this Petri net as
     * {@link ObservableList}.
     * 
     * @return {@link Place}s as {@link ObservableList}
     */
    public ObservableList<Place> getPlaces() {
        return places;
    }

    /**
     * This method sets the {@link Place}s of this Petri net to the given ones.
     * 
     * @param places
     *            {@link Place}s as {@link List}
     */
    public void setPlaces(List<Place> places) {
        this.places.clear();
        this.places.addAll(places);
    }

    /**
     * This method adds the given {@link Place} to this Petri net.
     * 
     * @param place
     *            {@link Place} to add
     */
    public void addPlace(Place place) {
        places.add(place);
    }

    /**
     * This method returns the {@link Edge}s of this Petri net as
     * {@link ObservableList}.
     * 
     * @return {@link Edge}s as {@link ObservableList}
     */
    public ObservableList<Edge> getEdges() {
        return edges;
    }

    /**
     * This method sets the {@link Edge}s of this Petri net to the given ones.
     * 
     * @param edges
     *            {@link Edge}s as {@link List}
     */
    public void setEdges(List<Edge> edges) {
        this.edges.clear();
        this.edges.addAll(edges);
    }

    /**
     * This method adds the given {@link Edge} to this Petri net.
     * 
     * @param edge
     *            {@link Edge} to add
     */
    public void addEdge(Edge edge) {
        edges.add(edge);
    }

    /**
     * This method creates a new temporary {@link PetriNet}.
     * 
     * @return new temporary {@link PetriNet}
     */
    public static PetriNet createTempPetriNet() {
        try {
            PetriNet net = new PetriNet(File.createTempFile("mopebsPetriNet",
                    ".pnml"));
            NoOffsetPNMLWriter writer = new NoOffsetPNMLWriter(net);
            writer.save();
            return net;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
