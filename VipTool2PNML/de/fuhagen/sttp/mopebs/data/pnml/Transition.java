package de.fuhagen.sttp.mopebs.data.pnml;

import de.fuhagen.sttp.mopebs.data.graph.Node;

/**
 * This class represents a {@link Transition} of a {@link PetriNet}.
 * 
 * @author thomas
 */
public class Transition extends Node {

    /**
     * Border length of the transition visualization.
     */
    public static final int TRANSITION_SIZE = 32;

    /**
     * Creates a new {@link Transition} with label "Transition".
     */
    public Transition() {
        super();

        setWidth(TRANSITION_SIZE);
        setHeigth(TRANSITION_SIZE);
        setOffsetX(0);
        setOffsetY(0);
        setLabel("Transition");
    }

}
