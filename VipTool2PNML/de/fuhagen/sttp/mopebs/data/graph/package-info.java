/**
 * This package contains general graph structure classes which can be used to
 * quickly create new graph types.
 * 
 * @author thomas
 */
package de.fuhagen.sttp.mopebs.data.graph;