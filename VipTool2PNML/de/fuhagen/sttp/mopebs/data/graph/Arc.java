package de.fuhagen.sttp.mopebs.data.graph;

import java.util.Collections;
import java.util.List;
import java.util.Vector;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 * This class represents a directed graph arc between to graph nodes.
 * 
 * @author thomas
 */
public class Arc {

    /**
     * Helper for parsing: ID of start node.
     */
    public String                            startid    = null;
    /**
     * Helper for parsing: ID of end node.
     */
    public String                            endid      = null;
    /**
     * Helper for parsing: X coordinates of drag points.
     */
    public Vector<Integer>                   dragPointX = new Vector<Integer>();
    /**
     * Helper for parsing: Y coordinates of drag points.
     */
    public Vector<Integer>                   dragPointY = new Vector<Integer>();

    /**
     * ID of this arc.
     */
    private final StringProperty             id;

    /**
     * Start node of arc.
     */
    private final SimpleObjectProperty<Node> start;
    /**
     * End node of arc.
     */
    private final SimpleObjectProperty<Node> end;

    /**
     * Arc inscription.
     */
    private final StringProperty             inscription;
    /**
     * User defined x offset of inscription.
     */
    private final IntegerProperty            offsetx;
    /**
     * User defined y offset of inscription.
     */
    private final IntegerProperty            offsety;

    /**
     * Weight of arc.
     */
    private final IntegerProperty            weight;

    /**
     * List of all {@link DragPoint}s of arc.
     */
    private final ObservableList<DragPoint>  dragpoints;

    /**
     * Creates a new arc.
     */
    public Arc() {
        id = new SimpleStringProperty("Node" + System.currentTimeMillis());

        start = new SimpleObjectProperty<Node>();
        end = new SimpleObjectProperty<Node>();

        inscription = new SimpleStringProperty("");
        offsetx = new SimpleIntegerProperty(0);
        offsety = new SimpleIntegerProperty(0);

        weight = new SimpleIntegerProperty(1);

        dragpoints = FXCollections.observableArrayList();
    }

    /**
     * This method returns the ID of this arc.
     * 
     * @return ID as {@link String}
     */
    public String getId() {
        return id.get();
    }

    /**
     * This method returns the ID as {@link StringProperty}.
     * 
     * @return ID as {@link StringProperty}
     */
    public StringProperty getIdProperty() {
        return id;
    }

    /**
     * This method returns the start {@link Node} of this arc.
     * 
     * @return start {@link Node}
     */
    public Node getStart() {
        return start.get();
    }

    /**
     * This method sets the start {@link Node} of this arc.
     * 
     * @param start
     *            start {@link Node}
     */
    public void setStart(Node start) {
        this.start.setValue(start);
    }

    /**
     * This method returns the start {@link Node} as
     * {@link SimpleObjectProperty}.
     * 
     * @return start {@link Node} as {@link SimpleObjectProperty}
     */
    public SimpleObjectProperty<Node> getStartProperty() {
        return start;
    }

    /**
     * This method returns the end {@link Node} of this arc.
     * 
     * @return end {@link Node}
     */
    public Node getEnd() {
        return end.get();
    }

    /**
     * This method sets the end {@link Node} of this arc.
     * 
     * @param end
     *            end {@link Node}
     */
    public void setEnd(Node end) {
        this.end.setValue(end);
    }

    /**
     * This method returns the end {@link Node} as {@link SimpleObjectProperty}.
     * 
     * @return end {@link Node} as {@link SimpleObjectProperty}
     */
    public SimpleObjectProperty<Node> getEndProperty() {
        return end;
    }

    /**
     * This method returns the user defined x offset of the inscription label.
     * 
     * @return x offset as {@link Integer}
     */
    public int getOffsetX() {
        return offsetx.getValue();
    }

    /**
     * This method set the user defined x offset of the inscription label.
     * 
     * @param offsetx
     *            x offset as {@link Integer}
     */
    public void setOffsetX(int offsetx) {
        this.offsetx.set(offsetx);
    }

    /**
     * This method returns the user defined x offset of the inscription label as
     * {@link IntegerProperty}.
     * 
     * @return x offset as {@link IntegerProperty}
     */
    public IntegerProperty getOffsetXProperty() {
        return offsetx;
    }

    /**
     * This method returns the user defined y offset of the inscription label.
     * 
     * @return y offset as {@link Integer}
     */
    public int getOffsetY() {
        return offsety.getValue();
    }

    /**
     * This method set the user defined y offset of the inscription label.
     * 
     * @param offsety
     *            y offset as {@link Integer}
     */
    public void setOffsetY(int offsety) {
        this.offsety.set(offsety);
    }

    /**
     * This method returns the user defined y offset of the inscription label as
     * {@link IntegerProperty}.
     * 
     * @return y offset as {@link IntegerProperty}
     */
    public IntegerProperty getOffsetYProperty() {
        return offsety;
    }

    /**
     * This method returns the inscription of this arc as {@link String}.
     * 
     * @return inscription {@link String}
     */
    public String getInscription() {
        return inscription.getName();
    }

    /**
     * This method set the inscription of this arc..
     * 
     * @param inscription
     *            inscription as {@link String}
     */
    public void setInscription(String inscription) {
        this.inscription.set(inscription);
    }

    /**
     * This method returns the inscription of this arc as {@link StringProperty}
     * .
     * 
     * @return inscription as {@link StringProperty}
     */
    public StringProperty getInscriptionProperty() {
        return inscription;
    }

    /**
     * This method returns the weight of this arc.
     * 
     * @return weight as {@link Integer}
     */
    public int getWeight() {
        return weight.getValue();
    }

    /**
     * This method set the weight of this arc.
     * 
     * @param weight
     *            weight as {@link Integer}
     */
    public void setWeight(int weight) {
        this.weight.set(weight);
    }

    /**
     * This method returns the weight of this arc as {@link IntegerProperty}.
     * 
     * @return weight as {@link IntegerProperty}
     */
    public IntegerProperty getWeightProperty() {
        return weight;
    }

    /**
     * This method returns the {@link DragPoint}s of this arc.
     * 
     * @return {@link DragPoint}s as {@link ObservableList}
     */
    public ObservableList<DragPoint> getDragPoints() {
        return dragpoints;
    }

    /**
     * This method returns the {@link DragPoint}s of this arc as sorted
     * {@link Vector}.
     * 
     * @return {@link DragPoint}s from start to end as {@link Vector}
     */
    public Vector<DragPoint> getSortedDragPoints() {
        Vector<DragPoint> sorted = new Vector<DragPoint>(dragpoints);
        Collections.sort(sorted,
                (d1, d2) -> (d1.getOrder() > d2.getOrder()) ? 1 : -1);
        return sorted;
    }

    /**
     * This method replace the {@link DragPoint}s of this arc with the given
     * ones.
     * 
     * @param dragPoints
     *            {@link DragPoint}s of arc
     */
    public void setDragPoints(List<DragPoint> dragPoints) {
        this.dragpoints.clear();
        this.dragpoints.addAll(dragPoints);
    }

    /**
     * This method adds a new {@link DragPoint} to this arc.
     * 
     * @param dragPoint
     *            new {@link DragPoint}
     */
    public void addDragPoint(DragPoint dragPoint) {
        dragpoints.add(dragPoint);
    }
}
