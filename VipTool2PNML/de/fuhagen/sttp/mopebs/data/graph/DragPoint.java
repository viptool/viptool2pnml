package de.fuhagen.sttp.mopebs.data.graph;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;

/**
 * This class represents a drag point of an arc.
 * 
 * @author thomas
 */
public class DragPoint {

    /**
     * X coordinate.
     */
    private IntegerProperty x;
    /**
     * Y coordinate.
     */
    private IntegerProperty y;
    /**
     * Order of drag point.
     */
    private DoubleProperty  order;

    /**
     * This method creates a new drag point at the given position with the given
     * order.
     * 
     * @param x
     *            x coordinate
     * @param y
     *            y coordinate
     * @param order
     *            order
     */
    public DragPoint(int x, int y, double order) {
        this.x = new SimpleIntegerProperty(x);
        this.y = new SimpleIntegerProperty(y);
        this.order = new SimpleDoubleProperty(order);
    }

    /**
     * This method returns the x coordinate of this drag point.
     * 
     * @return x coordinate as {@link Integer}
     */
    public int getX() {
        return x.get();
    }

    /**
     * This method sets the x coordinate of this drag point.
     * 
     * @param x
     *            x coordinate
     */
    public void setX(int x) {
        this.x.set(x);
    }

    /**
     * This method returns the x coordinate of this drag point as
     * {@link IntegerProperty}.
     * 
     * @return x coordinate as {@link IntegerProperty}
     */
    public IntegerProperty getXProperty() {
        return x;
    }

    /**
     * This method returns the y coordinate of this drag point.
     * 
     * @return y coordinate as {@link Integer}
     */
    public int getY() {
        return y.get();
    }

    /**
     * This method sets the y coordinate of this drag point.
     * 
     * @param y
     *            y coordinate
     */
    public void setY(int y) {
        this.y.set(y);
    }

    /**
     * This method returns the y coordinate of this drag point as
     * {@link IntegerProperty}.
     * 
     * @return y coordinate as {@link IntegerProperty}
     */
    public IntegerProperty getYProperty() {
        return y;
    }

    /**
     * This method returns order of this drag point.
     * 
     * @return order as {@link Double}
     */
    public double getOrder() {
        return order.get();
    }

    /**
     * This method sets the order of this drag point.
     * 
     * @param order
     *            order as {@link Double}
     */
    public void setOrder(double order) {
        this.order.set(order);
    }

    /**
     * This method returns the order of this drag point as
     * {@link DoubleProperty}.
     * 
     * @return order as {@link DoubleProperty}
     */
    public DoubleProperty getOrderProperty() {
        return order;
    }
}
