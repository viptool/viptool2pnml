package de.fuhagen.sttp.mopebs.data.graph;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * This class represents a node of a graph.
 * 
 * @author thomas
 */
public class Node {

    /**
     * ID of node.
     */
    private final StringProperty  id;

    /**
     * Label of node.
     */
    private final StringProperty  label;
    /**
     * X offset of node label.
     */
    private final IntegerProperty offsetx;
    /**
     * Y offset of node label.
     */
    private final IntegerProperty offsety;

    /**
     * X coordinate of node.
     */
    private final IntegerProperty x;
    /**
     * Y coordinate of node.
     */
    private final IntegerProperty y;

    /**
     * Node width.
     */
    private final IntegerProperty width;
    /**
     * Node height.
     */
    private final IntegerProperty height;

    /**
     * Creates a new node.
     */
    public Node() {
        id = new SimpleStringProperty("Node" + System.currentTimeMillis() + ""
                + (Math.random() * 10000));

        label = new SimpleStringProperty("node");
        offsetx = new SimpleIntegerProperty(0);
        offsety = new SimpleIntegerProperty(0);

        x = new SimpleIntegerProperty(0);
        y = new SimpleIntegerProperty(0);

        width = new SimpleIntegerProperty(0);
        height = new SimpleIntegerProperty(0);
    }

    /**
     * Returns the node ID.
     * 
     * @return ID as {@link String}
     */
    public String getId() {
        return id.get();
    }

    /**
     * Returns the node ID as {@link StringProperty}
     * 
     * @return ID as {@link StringProperty}
     */
    public StringProperty getIdProperty() {
        return id;
    }

    /**
     * Returns the label.
     * 
     * @return label as {@link String}
     */
    public String getLabel() {
        return label.get();
    }

    /**
     * Set label to the given {@link String}.
     * 
     * @param label
     *            label as {@link String}
     */
    public void setLabel(String label) {
        this.label.set(label);
    }

    /**
     * This method returns the label as {@link StringProperty}
     * 
     * @return label as {@link StringProperty}
     */
    public StringProperty getLabelProperty() {
        return label;
    }

    /**
     * Returns the x coordinate.
     * 
     * @return x coordinate as {@link Integer}
     */
    public int getX() {
        return x.get();
    }

    /**
     * This method sets the x coordinate of this node.
     * 
     * @param x
     *            x coordinate
     */
    public void setX(int x) {
        this.x.set(x);
    }

    /**
     * This method returns the x coordinate as {@link IntegerProperty}.
     * 
     * @return x coordinate as {@link IntegerProperty}
     */
    public IntegerProperty getXProperty() {
        return x;
    }

    /**
     * Returns the y coordinate.
     * 
     * @return y coordinate as {@link Integer}
     */
    public int getY() {
        return y.get();
    }

    /**
     * This method sets the y coordinate of this node.
     * 
     * @param y
     *            y coordinate
     */
    public void setY(int y) {
        this.y.set(y);
    }

    /**
     * This method returns the y coordinate as {@link IntegerProperty}.
     * 
     * @return y coordinate as {@link IntegerProperty}
     */
    public IntegerProperty getYProperty() {
        return y;
    }

    /**
     * Returns the user defined x offset of the node label.
     * 
     * @return x offset as {@link Integer}
     */
    public int getOffsetX() {
        return offsetx.get();
    }

    /**
     * This method sets the x offset of the node label.
     * 
     * @param offsetx
     *            x offset
     */
    public void setOffsetX(int offsetx) {
        this.offsetx.set(offsetx);
    }

    /**
     * This method returns the x offset of the node label as
     * {@link IntegerProperty}.
     * 
     * @return x offset as {@link IntegerProperty}
     */
    public IntegerProperty getOffsetXProperty() {
        return offsetx;
    }

    /**
     * Returns the user defined y offset of the node label.
     * 
     * @return y offset as {@link Integer}
     */
    public int getOffsetY() {
        return offsety.get();
    }

    /**
     * This method sets the y offset of the node label.
     * 
     * @param offsety
     *            y offset
     */
    public void setOffsetY(int offsety) {
        this.offsety.set(offsety);
    }

    /**
     * This method returns the y offset of the node label as
     * {@link IntegerProperty}.
     * 
     * @return y offset as {@link IntegerProperty}
     */
    public IntegerProperty getOffsetYProperty() {
        return offsety;
    }

    /**
     * Returns the width of the node.
     * 
     * @return width as {@link Integer}
     */
    public int getWidth() {
        return width.get();
    }

    /**
     * This method sets the width of the node.
     * 
     * @param width
     *            node width
     */
    public void setWidth(int width) {
        this.width.set(width);
    }

    /**
     * This method returns the node width as {@link IntegerProperty}.
     * 
     * @return width as {@link IntegerProperty}
     */
    public IntegerProperty getWidthProperty() {
        return width;
    }

    /**
     * Returns the height of the node.
     * 
     * @return height as {@link Integer}
     */
    public int getHeight() {
        return height.get();
    }

    /**
     * This method sets the height of the node.
     * 
     * @param height
     *            node height
     */
    public void setHeigth(int height) {
        this.height.set(height);
    }

    /**
     * This method returns the node height as {@link IntegerProperty}.
     * 
     * @return height as {@link IntegerProperty}
     */
    public IntegerProperty getHeightProperty() {
        return height;
    }
}
