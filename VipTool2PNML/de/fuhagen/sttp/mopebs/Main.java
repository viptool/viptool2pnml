package de.fuhagen.sttp.mopebs;

import java.io.File;
import java.util.Vector;

import javafx.application.Application;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import de.fuhagen.sttp.mopebs.data.pnml.PetriNet;
import de.fuhagen.sttp.mopebs.data.pnml.parser.PNMLParser;
import de.fuhagen.sttp.mopebs.data.pnml.writer.NoOffsetPNMLWriter;

public class Main extends Application {

	@Override
	public void start(Stage primaryStage) {
		FileChooser fileChooser = new FileChooser();
		fileChooser.setTitle("VipTool .pnml file");
		FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter(
				"*.pnml", "*.pnml");
		fileChooser.getExtensionFilters().add(extFilter);

		File file = fileChooser.showOpenDialog(null);
		if (file != null && file.exists()) {
			PNMLParser parser = new PNMLParser(file);
			parser.parse();
			Vector<PetriNet> nets = parser.getNets();
			if (!nets.isEmpty()) {
				PetriNet net = nets.firstElement();

				fileChooser = new FileChooser();
				fileChooser.setInitialDirectory(file.getParentFile());
				fileChooser.setTitle("Save as WoPeD .pnml file");
				extFilter = new FileChooser.ExtensionFilter("*.pnml", "*.pnml");
				fileChooser.getExtensionFilters().add(extFilter);

				File out = fileChooser.showSaveDialog(null);

				if (out != null) {
					NoOffsetPNMLWriter writer = new NoOffsetPNMLWriter(net, out);
					writer.save();
				}
			}
		}

		System.exit(0);
	}

	public static void main(String[] args) {
		launch(args);
	}

}
