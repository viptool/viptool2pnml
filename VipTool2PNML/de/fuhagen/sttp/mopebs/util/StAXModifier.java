package de.fuhagen.sttp.mopebs.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.StringReader;

import javax.xml.stream.XMLEventFactory;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLEventWriter;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.events.XMLEvent;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;

/**
 * Parses through an XML file and changes it on the fly.
 */
public abstract class StAXModifier {

    /**
     * Reference to XML event factory.
     */
    private static XMLEventFactory eventFactory = null;

    /**
     * Buffer for modified XML file.
     */
    private StringBuffer           buffer       = null;

    /**
     * File to parse.
     */
    private File                   file         = null;

    /**
     * Creates a new {@link StAXModifier} for the given {@link File}.
     * 
     * @param xmlfile
     *            file to parse
     */
    public StAXModifier(final File xmlfile) {
        super();
        this.file = xmlfile;
        StAXModifier.eventFactory = XMLEventFactory.newInstance();
    }

    /**
     * Returns a reference to the XML event factory.
     * 
     * @return {@link XMLEventFactory}
     */
    public final XMLEventFactory getEventFactory() {
        return StAXModifier.eventFactory;
    }

    /**
     * Implement this to catch the Events your implementation shall react to. If
     * the implementation catches an event which it actually modifies the method
     * must return true, otherwise false.
     * 
     * @param event
     *            the event that triggered this method call
     * @param writer
     *            the writer that should be used for your event-handlers output
     * @return true if the method implementation has reacted to the event
     */
    public abstract boolean modifyEvent(XMLEvent event, XMLEventWriter writer);

    /**
     * Read XML file into buffer.
     */
    public final void readFile() {
        try {
            final BufferedReader freader = new BufferedReader(new FileReader(
                    file));
            buffer = new StringBuffer();

            String line = null;
            while ((line = freader.readLine()) != null) {
                buffer.append(line + "\n");
            }

            freader.close();
        } catch (final IOException e) {
            e.printStackTrace();
        }

    }

    /**
     * Write modified file.
     */
    public final void writeFile() {
        try {
            Source source = new StreamSource(
                    new StringReader(buffer.toString()));

            XMLEventReader reader = XMLInputFactory.newFactory()
                    .createXMLEventReader(source);

            final XMLEventWriter writer = XMLOutputFactory.newFactory()
                    .createXMLEventWriter(new FileOutputStream(file, false));

            while (reader.hasNext()) {
                XMLEvent event = reader.nextEvent();

                if (!modifyEvent(event, writer)) {
                    writer.add(event);
                }
            }

            reader.close();
            writer.flush();
            writer.close();

        } catch (final Exception e) {
            e.printStackTrace();
        }
    }

}
