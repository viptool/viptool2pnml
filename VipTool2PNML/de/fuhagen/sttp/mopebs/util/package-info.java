/**
 * This package contains all viptool utility classes.
 * 
 * @author thomas
 *
 */
package de.fuhagen.sttp.mopebs.util;