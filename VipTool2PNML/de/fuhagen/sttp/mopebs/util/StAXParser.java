package de.fuhagen.sttp.mopebs.util;

import java.io.File;
import java.io.FileInputStream;
import java.util.Iterator;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.Characters;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

/**
 * StAX Parser helper class. This class can be used to implement StaX parsers
 * for xml files.
 */
public abstract class StAXParser {

    /**
     * File to parse.
     */
    protected File file = null;

    /**
     * Creates a new StAXParser for the given file.
     * 
     * @param xmlfile
     *            file to parse as {@link File}
     */
    public StAXParser(final File xmlfile) {
        super();

        this.file = xmlfile;
    }

    /**
     * This method is called if a attribute is found.
     * 
     * @param attribute
     *            {@link Attribute}
     */
    public void attribute(final Attribute attribute) {

    }

    /**
     * This class is called if characters are found.
     * 
     * @param characters
     *            {@link Characters}
     */
    public void characters(final Characters characters) {

    }

    /**
     * End of element.
     * 
     * @param end
     *            {@link EndElement}
     */
    public void endElement(final EndElement end) {

    }

    /**
     * Parse file.
     */
    public final void parse() {
        try {
            XMLEventReader parser = XMLInputFactory.newInstance()
                    .createXMLEventReader(new FileInputStream(file));

            while (parser.hasNext()) {
                XMLEvent event = parser.nextEvent();
                handleEvent(event);
                if (event.getEventType() == XMLStreamConstants.END_DOCUMENT) {
                    parser.close();
                }
            }

        } catch (final Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Evaluate XML events.
     * 
     * @param event
     *            {@link XMLEvent}
     */
    private void handleEvent(final XMLEvent event) {
        switch (event.getEventType()) {
            case XMLStreamConstants.START_ELEMENT:
                handleStartEvent(event);
                break;
            case XMLStreamConstants.CHARACTERS:
                handleWhiteSpace(event);
                break;
            case XMLStreamConstants.END_ELEMENT:
                endElement(event.asEndElement());
                break;
            default:
        }
    }

    /**
     * Handle whitespace XML event.
     * 
     * @param event
     *            {@link XMLEvent}
     */
    private void handleWhiteSpace(final XMLEvent event) {
        Characters ch = event.asCharacters();
        if (!ch.isWhiteSpace()) {
            characters(ch);
        }
    }

    /**
     * Handle start element event.
     * 
     * @param event
     *            {@link XMLEvent}
     */
    private void handleStartEvent(final XMLEvent event) {
        StartElement element = event.asStartElement();
        startElement(element);
        handleAttributes(element.getAttributes());

    }

    /**
     * Handle XML attribute.
     * 
     * @param attributes
     *            attribute iterator
     */
    private void handleAttributes(final Iterator<?> attributes) {
        while (attributes.hasNext()) {
            Attribute attr = (Attribute) attributes.next();
            attribute(attr);
        }
    }

    /**
     * Start of element.
     * 
     * @param start
     *            {@link StartElement}
     */
    public abstract void startElement(StartElement start);
}
