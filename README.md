# README #

Converter for buggy VipTool 6.0.3 pnml format to (WoPeD and ProM) compatible PNML format.

### Required ###

* Java 8 (at least u31)

### Usage ###

* Run (java -jar VipTool2PNML.jar)
* Select VipTool 6.0.3 pnml Petri net file
* Choose a name for the converted new file
* Done. Open the new file with WoPeD / ProM

### Who do I talk to? ###

* thomas.irgang@fernuni-hagen.de
